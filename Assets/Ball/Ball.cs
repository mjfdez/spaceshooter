﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public Vector2 speed;


	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    void Update() {
        transform.Translate(speed * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
         
        switch (other.tag)
        {
            case "right":
            case "left":
                speed.x = -speed.x;
                break;
            case "down":
            case "up":
                speed.y = -speed.y;
                break;
            case "Player":
                speed.y = -speed.y;
                break;
            case "bricks":
                other.gameObject.GetComponent<bricks>().Touch();
                speed.y = -speed.y;
                break;
        }

	}
}
